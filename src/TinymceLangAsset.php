<?php
/**
 * Copyright  (C) 8/2/22, 12:21 AM , Inc - All Rights Reserved
 * Author: Beget
 * Programmer: Mohamad Kazem Lotfi
 * Phone : +989130012987
 * WebSite : https://beget.ir
 * Powered By : Yii2  (https://www.yiiframework.com)
 *
 */


/**
 * @copyright Copyright (c) 2013-2017 Sajflow Services
 * @link https://www.sajflow.com
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

namespace begetir\tinymce;

use yii\web\AssetBundle;

class TinymceLangAsset extends AssetBundle
{
    public $sourcePath = '@vendor/begetir/yii2-tinymce/src/assets';

    public $depends = [
        'begetir\tinymce\WidgetAsset'
    ];
}

