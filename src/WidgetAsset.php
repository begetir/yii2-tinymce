<?php
/**
 * Copyright  (C) 8/2/22, 12:16 AM , Inc - All Rights Reserved
 * Author: Beget
 * Programmer: Mohamad Kazem Lotfi
 * Phone : +989130012987
 * WebSite : https://beget.ir
 * Powered By : Yii2  (https://www.yiiframework.com)
 *
 */

namespace begetir\tinymce;


use yii\web\AssetBundle;

class WidgetAsset extends AssetBundle
{
    public $sourcePath = '@vendor/tinymce/tinymce';

    public function init()
    {
        parent::init();
        $this->js[] = YII_DEBUG ? 'tinymce.js' : 'tinymce.min.js';
    }
}

